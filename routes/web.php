<?php

use App\Http\Controllers\HowToController;
use App\Models\HowTo;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard')->with([
            "howtos" => HowTo::all()
        ]);
    })->name('dashboard');

    Route::post('/redirect', [HowToController::class, 'store']);
});

Route::get('/{any}',function ($uri) {
    return Redirect::to(HowTo::where('uri', $uri)->firstOrFail()->link);
})->where('any', '.*');

