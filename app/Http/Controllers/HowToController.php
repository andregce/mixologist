<?php

namespace App\Http\Controllers;

use App\Http\Requests\HowTo\StoreRequest;
use App\Models\HowTo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class HowToController extends Controller
{
    const FILLABLES = [
        'uri',
        'link',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        HowTo::create($request->only(self::FILLABLES));
        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param HowTo $howTo
     * @return Response
     */
    public function show(HowTo $howTo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param HowTo $howTo
     * @return Response
     */
    public function edit(HowTo $howTo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param HowTo $howTo
     * @return Response
     */
    public function update(Request $request, HowTo $howTo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param HowTo $howTo
     * @return Response
     */
    public function destroy(HowTo $howTo)
    {
        //
    }
}
