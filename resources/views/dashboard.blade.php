<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ url('redirect') }}">
                        @csrf
                        <div class="mb-5 flex justify-end">
                            <x-button class="ml-3">
                                {{ __('Add') }}
                            </x-button>
                        </div>
                        <div class="grid grid-cols-2 gap-4 mb-5">
                            <!-- From -->
                            <div>
                                <x-input id="uri" class="block mt-1 w-full" name="uri" :value="old('uri')" required autofocus />
                            </div>

                            <!-- To -->
                            <div>
                                <x-input id="link" class="block mt-1 w-full" name="link" required />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    @foreach($howtos as $howto)
                        <div class="grid grid-cols-2 gap-4 mb-5">
                            <!-- From -->
                            <div>
                                {{ $howto->uri }}
                            </div>

                            <!-- To -->
                            <div>
                                {{ $howto->link }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
